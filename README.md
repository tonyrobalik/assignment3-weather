This application was developed from scratch as an assignment for a verified-track course on Coursera titled [**Programming Mobile Services for Android Handheld Systems: Communication**](https://www.coursera.org/course/posacommunication)

The purpose of this exercise was to gain experience with AIDL and bound Services. It is not production-ready code.