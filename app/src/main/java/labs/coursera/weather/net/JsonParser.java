package labs.coursera.weather.net;

import android.support.annotation.NonNull;
import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import labs.coursera.weather.aidl.WeatherData;

/**
 * Created by Tony on 5/26/2015.
 */
public final class JsonParser {
    private static final String TAG = JsonParser.class.getSimpleName();

    @NonNull
    public static List<WeatherData> readJsonStream(@NonNull InputStream in) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

        // Returning a list to match API specification
        List<WeatherData> wrapper = new ArrayList<>();
        try {
            try {
                // We only get the first element in the potential list because that's all we care about
                wrapper.add(readWeatherData(reader));
                //return wrapper;
            } finally {
                reader.close();
            }
        } catch (IllegalStateException e) {
            //return wrapper;
            Log.e(TAG, "Encountered an illegal state");
        }

        return wrapper;
    }

    @NonNull
    private static WeatherData readWeatherData(@NonNull JsonReader reader)
            throws IOException, IllegalStateException {
        String name = null; // place name
        double[] wind = null; // speed, deg
        Main main = null; // temp, humidity
        long[] sys = null; // sunrise, sunset

        reader.beginObject();
        while (reader.hasNext()) {
            String token = reader.nextName();
            switch (token) {
                case "wind":
                    wind = readWind(reader);
                    break;
                case "main":
                    main = readMain(reader);
                    break;
                case "sys":
                    sys = readSys(reader);
                    break;
                case "name":
                    name = reader.nextString();
                    break;
                default:
                    reader.skipValue();
            }
        }
        reader.endObject();

        if (name == null || wind == null || main == null || sys == null) {
            throw new IllegalStateException("JSON data is bad and cannot be parsed");
        }

        return new WeatherData(name, wind[0], wind[1], main.temp, main.humidity, sys[0], sys[1]);
    }

    @NonNull
    private static double[] readWind(@NonNull JsonReader reader) throws IOException {
        double[] wind = new double[2];

        reader.beginObject();
        while (reader.hasNext()) {
            String token = reader.nextName();
            switch (token) {
                case "speed":
                    wind[0] = reader.nextDouble();
                    break;
                case "deg":
                    wind[1] = reader.nextDouble();
                    break;
                default:
                    reader.skipValue();
            }
        }
        reader.endObject();

        return wind;
    }

    @NonNull
    private static Main readMain(@NonNull JsonReader reader) throws IOException {
        Main main = new Main();

        reader.beginObject();
        while (reader.hasNext()) {
            String token = reader.nextName();
            switch (token) {
                case "temp":
                    main.temp = reader.nextDouble();
                    break;
                case "humidity":
                    main.humidity = reader.nextLong();
                    break;
                default:
                    reader.skipValue();
            }
        }
        reader.endObject();

        return main;
    }

    @NonNull
    private static long[] readSys(@NonNull JsonReader reader) throws IOException {
        long[] sys = new long[2];

        reader.beginObject();
        while (reader.hasNext()) {
            String token = reader.nextName();
            switch (token) {
                case "sunrise":
                    sys[0] = reader.nextLong();
                    break;
                case "sunset":
                    sys[1] = reader.nextLong();
                    break;
                default:
                    reader.skipValue();
            }
        }
        reader.endObject();

        return sys;
    }

    private static class Main {
        private double temp;
        private long humidity;
    }
}