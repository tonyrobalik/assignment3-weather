package labs.coursera.weather.net;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import labs.coursera.weather.aidl.WeatherData;
import labs.coursera.weather.model.TimedCache;

/**
 * Created by Tony on 5/26/2015.
 * Creates a connection to the OpenWeatherMap.org API and then parses data on request using
 * a JsonReader.
 */
public final class OpenWeatherMapServer {
    private static final String TAG = OpenWeatherMapServer.class.getSimpleName();

    private static final String OPEN_WEATHER_MAP_URL = "http://api.openweathermap.org/data/2.5/weather?q=";

    // Private constructor to prevent instantiation
    private OpenWeatherMapServer() {
    }

    // Cache
    private static TimedCache sCache;

    /**
     * Returns the latest weather relating to the search parameter, or null if there is an exception
     * or the search term is invalid. This method is synchronized to prevent concurrent access
     * to this "server." There might be a more efficient way to make this class thread-safe.
     *
     * @param search The search phrase
     * @return The latest weather wrapped in a List, or null if there is an exception or the search
     * term is invalid.
     * @throws IOException
     */
    @Nullable
    public synchronized static List<WeatherData> getWeather(@NonNull String search) throws IOException {
        Log.d(TAG, String.format("getWeather(%s)", search));

        // Get data from cache
        List<WeatherData> latest = checkCache(search);

        // If cache value is valid, return that
        if (latest != null) {
            Log.d(TAG, "sCache returned valid weather data; returning it");
            return latest;
        }

        // Otherwise, get new data from server
        latest = getWeatherFromServer(search);

        // Update cache with new data
        updateCache(search, latest);

        return latest;
    }

    /**
     * Returns the latest weather, if it's still valid; null otherwise.
     *
     * @param search The search term to use as a key when checking the cache
     * @return The latest weather, if it's valid; null otherwise
     */
    @Nullable
    private static List<WeatherData> checkCache(@NonNull String search) {
        Log.d(TAG, String.format("checkCache(%s)", search));

        if (sCache == null) {
            Log.d(TAG, "sCache is null; returning null");
            return null;
        }
        return sCache.getWeatherIfValid(search); // could be null
    }

    /**
     * Updates the cache with the latest weather data.
     *
     * @param search The search term to use as a key when instantiating a new {@link TimedCache}
     *               object
     * @param latest The latest weather data
     */
    private static void updateCache(@NonNull String search, @Nullable List<WeatherData> latest) {
        if (latest == null || latest.size() == 0) return;
        sCache = new TimedCache(search, latest);
    }

    /**
     * Will return either a list of WeatherData objects, or null if there was an Exception.
     *
     * @param search Search term for OpenWeatherMap API
     * @return A List of WeatherData objects, or null if there was an Exception
     * @throws IOException Throws IOException if there was an error reading data from the server
     */
    @Nullable
    private static List<WeatherData> getWeatherFromServer(@NonNull String search)
            throws IOException {

        Log.d(TAG, String.format("getWeatherFromServer(%s)", search));

        List<WeatherData> list = null;

        try {
            HttpURLConnection urlConnection = null;
            try {
                //String request = URLEncoder.encode(OPEN_WEATHER_MAP_URL + search + apiKey(), "UTF-8");
                String query = URLEncoder.encode(search, "UTF-8") + apiKey();
                String request = OPEN_WEATHER_MAP_URL + query;
                URL url = new URL(request);
                urlConnection = (HttpURLConnection) url.openConnection();

                int response = urlConnection.getResponseCode();
                Log.d(TAG, "response code is " + response);
                if (response == 200) {
                    Log.d(TAG, "response was OK; parsing");
                    list = JsonParser.readJsonStream(urlConnection.getInputStream());
                }
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
        } catch (UnsupportedEncodingException | MalformedURLException e) {
            e.printStackTrace();
            Log.e(TAG, "Error encoding URL");
            return null;
        }

        return list;
    }

    /**
     * Returns the API key for use with the OpenWeatherMap API, with the query term prepended.
     * This needs to be revised to propertly obfuscate the key.
     *
     * @return The API key, with the HTML query prepended
     */
    @NonNull
    private static String apiKey() {
        // TODO obfuscate this
        return "&APPID=19c3865652d7dff5dce5be47db77e086";
    }
}