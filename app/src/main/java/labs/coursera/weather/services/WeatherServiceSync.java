package labs.coursera.weather.services;

import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import labs.coursera.weather.aidl.WeatherCall;
import labs.coursera.weather.aidl.WeatherData;
import labs.coursera.weather.net.OpenWeatherMapServer;

/**
 * Created by teyesahr on 5/26/15.
 */
public final class WeatherServiceSync extends LifecycleLoggingService {
    private static final String TAG = WeatherServiceSync.class.getSimpleName();

    @Override @NonNull
    public IBinder onBind(Intent intent) {
        super.onBind(intent);
        return mBinder;
    }

    private final WeatherCall.Stub mBinder = new WeatherCall.Stub() {
        @Override @Nullable
        public List<WeatherData> getCurrentWeather(@NonNull String weather)
                throws RemoteException {

            Log.d(TAG, String.format("WeatherCall.Stub.getCurrentWeather(%s)", weather));

            try {
                return OpenWeatherMapServer.getWeather(weather);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "IOException trying to get data from server");
                return null;
            }
        }
    };
}