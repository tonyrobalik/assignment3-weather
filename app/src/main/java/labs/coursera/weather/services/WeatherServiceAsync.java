package labs.coursera.weather.services;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import labs.coursera.weather.aidl.WeatherData;
import labs.coursera.weather.aidl.WeatherRequest;
import labs.coursera.weather.aidl.WeatherResults;
import labs.coursera.weather.net.OpenWeatherMapServer;

/**
 * Created by teyesahr on 5/26/15.
 */
public final class WeatherServiceAsync extends LifecycleLoggingService {
    private static final String TAG = WeatherServiceAsync.class.getSimpleName();

    @Override @NonNull
    public IBinder onBind(Intent intent) {
        super.onBind(intent);
        return mBinder;
    }

    private final WeatherRequest.Stub mBinder = new WeatherRequest.Stub() {
        @Override
        public void getCurrentWeather(@NonNull String weather, @NonNull WeatherResults results)
                throws RemoteException {
            Log.d(TAG, String.format("WeatherRequest.Stub.getCurrentWeather(%s, callback)", weather));

            // We're on the main (UI) thread, so use an AsyncTask
            if (Looper.getMainLooper().equals(Looper.myLooper())) {
                Log.d(TAG, "In UI thread, so instantiating and executing an AsyncTask");
                new MyAsyncTask(results).execute(weather);
            }
            // We're in a separate thread (besides the UI thread),
            // so it's safe to do this synchronously
            else {
                Log.d(TAG, "Not in UI thread, so making network connection synchronously");
                try {
                    results.sendResults(OpenWeatherMapServer.getWeather(weather));
                    Log.d(TAG, "results.sendResults() completed successfully");
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "IOException trying to get data from server");
                    results.sendResults(null);
                }
            }
        }
    };

    /**
     * Implementation of AsyncTask that makes a blocking call to OpenWeatherMapServer.getWeather()
     * and returns the data (via onPostExecute) to encapsulating class.
     */
    private class MyAsyncTask extends AsyncTask<String, Void, List<WeatherData>> {
        private final WeatherResults mResults;

        private MyAsyncTask(WeatherResults results) {
            mResults = results;
        }

        @Override
        @Nullable
        protected List<WeatherData> doInBackground(@NonNull String... params) {
            Log.d(TAG, "doInBackground()");
            try {
                return OpenWeatherMapServer.getWeather(params[0]);
            } catch (IOException e) {
                Log.e(TAG, "IOException: " + e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(@Nullable List<WeatherData> result) {
            Log.d(TAG, "onPostExecute()");
            try {
                mResults.sendResults(result);
            } catch (RemoteException e) {
                Log.e(TAG, "RemoteException: " + e.getMessage());
                throw new RuntimeException("RemoteException is fatal");
            }
        }
    }
}