package labs.coursera.weather.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import labs.coursera.weather.aidl.WeatherData;

/**
 * Created by teyesahr on 5/26/15.
 */

/**
 * This class is immutable
 */
public final class TimedCache {
    private static final String TAG = TimedCache.class.getSimpleName();

    /**
     * Time of creation of cache instance.
     */
    private final long mTime;

    /**
     * Cache will be valid for 10s after creation.
     */
    private static final long VALID_MAX = 10 * 1000; // 10s

    /**
     * Search term associated with this cache.
     */
    private final String mSearch;

    /**
     * Cached WeatherData.
     */
    private final List<WeatherData> mData;

    /**
     * Constructor. Requires non-null WeatherData object.
     * @param data A valid WeatherData object
     */
    public TimedCache(@NonNull String search, @NonNull List<WeatherData> data) {
        mSearch = search;
        mData = copy(data); // defensive copy
        mTime = System.currentTimeMillis();
    }

    /**
     * Returns latest WeatherData, if it is deemed valid. If not, returns null.
     * @return WeatherData if valid, otherwise null
     */
    @Nullable
    public List<WeatherData> getWeatherIfValid(@NonNull String search) {
        Log.d(TAG, String.format("getWeatherIfValid(%s)", search));

        if (!search.equals(mSearch) ||
                System.currentTimeMillis() > mTime + VALID_MAX) {
            // WeatherData no longer valid
            Log.d(TAG, "latest weather is invalid; returning null");
            return null;
        }

        Log.d(TAG, "latest weather is still valid; returning it");

        return copy(mData); // defensive copy
    }

    /**
     * Makes a defensive copy of the List of {@link WeatherData} objects.
     * @param data List to copy
     * @return A copy of the List of WeatherData objects
     */
    private List<WeatherData> copy(List<WeatherData> data) {
        List<WeatherData> list = new ArrayList<>(data.size());
        for (WeatherData w : data) {
            list.add(new WeatherData(w));
        }

        return list;
    }
}