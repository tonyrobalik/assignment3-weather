package labs.coursera.weather.ui;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import labs.coursera.weather.R;
import labs.coursera.weather.aidl.WeatherCall;
import labs.coursera.weather.aidl.WeatherData;
import labs.coursera.weather.aidl.WeatherRequest;
import labs.coursera.weather.aidl.WeatherResults;
import labs.coursera.weather.services.WeatherServiceAsync;
import labs.coursera.weather.services.WeatherServiceSync;

public final class WeatherActivity extends LifecycleLoggingActivity {

    private static final String TAG = WeatherActivity.class.getSimpleName();

    private EditText mSearchEdit;
    private TextView mWeatherText;
    private ProgressBar mDownloadProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        initViews();
    }

    /**
     * Get all Views used in this layout.
     */
    private void initViews() {
        mSearchEdit = (EditText) findViewById(R.id.edit_search);
        mWeatherText = (TextView) findViewById(R.id.text_weather_data);
        mDownloadProgress = (ProgressBar) findViewById(R.id.progress);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to both services
        bindServices();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from both services
        unbindServices();
    }

    // TODO override onResume and fetch latest WeatherData from TimedCache? (to account for reorientation)
    // TODO one day I'll get better at separating the business logic from the presentation code

    /**
     * Binds to both services only if they are not currently bound.
     */
    private void bindServices() {
        if (mServiceSync == null) {
            Log.d(TAG, "Binding to WeatherServiceSync");
            bindService(new Intent(getApplicationContext(), WeatherServiceSync.class),
                    mConnectionSync, BIND_AUTO_CREATE);
        }

        if (mServiceAsync == null) {
            Log.d(TAG, "Binding to WeatherServiceAsync");
            bindService(new Intent(getApplicationContext(), WeatherServiceAsync.class),
                    mConnectionAsync, BIND_AUTO_CREATE);
        }
    }

    /**
     * Helper method to unbind from both services.
     */
    private void unbindServices() {
        Log.d(TAG, "Unbinding from Services");
        unbindService(mConnectionSync);
        unbindService(mConnectionAsync);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_weather, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Responds to user clicking the sync button.
     *
     * @param v The sync button. Unused
     */
    public void onClickSync(@NonNull View v) {
        Log.d(TAG, "User clicked sync");
        onClick(true);
    }

    /**
     * Responds to user clicking the async button.
     *
     * @param v The async button. Unused
     */
    public void onClickAsync(@NonNull View v) {
        Log.d(TAG, "User clicked async");
        onClick(false);
    }

    /**
     * Responds to user click of either sync or async button.
     *
     * @param sync True if using synchronous service; false otherwise
     */
    private void onClick(boolean sync) {
        String search = getSearchText();

        if (isEmpty(search)) {
            notifyUser(getString(R.string.notify_empty_search));
            return;
        }

        getWeather(search, sync);
    }

    /**
     * Returns the search text from {@link #mSearchEdit}.
     *
     * @return The search string to use
     */
    @NonNull
    private String getSearchText() {
        return mSearchEdit.getText().toString();
    }

    /**
     * Returns true iff the string is null or empty.
     *
     * @param s The string to test for nullity or emptiness
     * @return True if the string is null or empty; false otherwise
     */
    private boolean isEmpty(@Nullable String s) {
        return s == null || s.equals("");
    }

    /**
     * Notifies the user of something.
     *
     * @param notification Notification string.
     */
    private void notifyUser(@NonNull String notification) {
        Toast.makeText(this, notification, Toast.LENGTH_LONG).show();
    }

    /**
     * Uses either WeatherServiceSync or WeatherServiceAsync, depending on value of boolean.
     *
     * @param search Search string to pass to Service
     * @param sync   Flag indicating which Service to use
     */
    private void getWeather(@NonNull String search, boolean sync) {
        Log.d(TAG, String.format("getWeather(%s, %s)", search, sync));

        try {
            showProgress(true);
            if (sync) {
                if (mServiceSync == null) {
                    Log.d(TAG, "WeatherServiceSync unbound");
                    notifyUser(getString(R.string.notify_service_unbound));
                    bindServices();
                    return;
                }

                // Instantiates and executes and AsyncTask to make a blocking call to
                // WeatherServiceSync
                Log.d(TAG, String.format("MyAsyncTask.execute(%s)", search));
                new MyAsyncTask().execute(search);
            } else {
                if (mServiceAsync == null) {
                    Log.d(TAG, "WeatherServiceAsync unbound");
                    notifyUser(getString(R.string.notify_service_unbound));
                    bindServices();
                    return;
                }
                Log.d(TAG, String.format("WeatherServiceAsync.getCurrentWeather(%s, callback)", search));
                mServiceAsync.getCurrentWeather(search, mWeatherResultsCallback);
            }
        } catch (RemoteException e) {
            Log.e(TAG, "RemoteException: " + e.getMessage());
            showProgress(false);
            e.printStackTrace();
        }
    }

    /**
     * Refreshes WeatherData in {@link #mWeatherText}.
     *
     * @param results List of WeatherData objects. This implementation will have just one item
     */
    private void refreshWeather(@Nullable List<WeatherData> results) {
        Log.d(TAG, "refreshWeather()");
        showProgress(false);

        if (results == null || results.size() == 0) {
            if (results == null) Log.d(TAG, "results is null");
            else if (results.size() == 0) Log.d(TAG, "results.size() == 0");

            notifyUser(getString(R.string.notify_no_results));
            mWeatherText.setText(getString(R.string.notify_no_results));
            return;
        }

        WeatherData weather = results.get(0);
        mWeatherText.setText(weather.toStringF());
    }

    /**
     * Helper method that hides/unhides ProgressBar and WeatherText as required.
     *
     * @param show True if {@link #mDownloadProgress} should be shown (and {@link #mWeatherText hidden});
     *             false otherwise
     */
    private void showProgress(boolean show) {
        if (show) {
            mDownloadProgress.setVisibility(View.VISIBLE);
            mWeatherText.setVisibility(View.GONE);
            mWeatherText.setText("");
        } else {
            mDownloadProgress.setVisibility(View.GONE);
            mWeatherText.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Callback object for use by asynchronous service
     */
    private final WeatherResults mWeatherResultsCallback = new WeatherResults.Stub() {
        @Override
        public void sendResults(final List<WeatherData> results) throws RemoteException {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refreshWeather(results);
                }
            });
        }
    };

    /**
     * Synchronous Service
     */
    WeatherCall mServiceSync;
    private final ServiceConnection mConnectionSync = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "Bound to WeatherServiceSync");
            mServiceSync = WeatherCall.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "Disconnected from WeatherServiceSync");
            mServiceSync = null;
        }
    };

    /**
     * Asynchronous Service
     */
    WeatherRequest mServiceAsync;
    private final ServiceConnection mConnectionAsync = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "Bound to WeatherServiceAsync");
            mServiceAsync = WeatherRequest.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "Disconnected from WeatherServiceAsync");
            mServiceAsync = null;
        }
    };

    /**
     * Implementation of AsyncTask that makes a blocking call to WeatherServiceSync and
     * returns the data (via onPostExecute) to encapsulating class.
     */
    private class MyAsyncTask extends AsyncTask<String, Void, List<WeatherData>> {
        @Override
        @Nullable
        protected List<WeatherData> doInBackground(@NonNull String... params) {
            Log.d(TAG, "doInBackground()");
            try {
                Log.d(TAG, String.format("mServiceSync.getCurrentWeather(%s)", params[0]));
                return mServiceSync.getCurrentWeather(params[0]);
            } catch (RemoteException e) {
                Log.e(TAG, "RemoteException: " + e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(@Nullable List<WeatherData> result) {
            Log.d(TAG, "onPostExecute()");
            refreshWeather(result);
        }
    }
}